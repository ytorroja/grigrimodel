#include <EEPROM.h>

#define NUMBER_OF_PARAMS 8

int getVel() {
  return vel * 100;
}

void setVel(int v) {
  vel = v / 100.0;
}

int getAmp() {
  return amp;
}

void setAmp(int a) {
  amp = a;
}

int getDec() {
  return dec * 1000;
}

void setDec(int d) {
  dec = d / 1000.0;
}

int getOff() {
  return offset;
}

void setOff(int o) {
  offset = o;
}

int getAcc() {
  return acc * 1000;
}

void setAcc(int a) {
  acc = a / 1000.0;
}

int getWsp() {
  return (wsp - 90) * 100.0 / 85.0;
}

void setWsp(int w) {
  wsp = w * 85.0 / 100.0 + 90;
}

boolean getMod() {
  return mod;
}

void setMod(int m) {
  mod = m;
}

int getTimeout() {
  return timeout_ms / 1000;
}

void setTimeout(int t) {
  timeout_ms = t * 1000;
}

void printParams() {
  Serial.print(F("Speed: "));
  Serial.println(getVel());
  Serial.print(F("Amplitude: "));
  Serial.println(getAmp());
  Serial.print(F("Decay: "));
  Serial.println(getDec());
  Serial.print(F("Offset: "));
  Serial.println(getOff());
  Serial.print(F("W.Speed: "));
  Serial.println(getWsp());
  Serial.print(F("Accel: "));
  Serial.println(getAcc());
  Serial.print(F("Mode: "));
  Serial.println((getMod() ? "LIN" : "SIN"));  
  Serial.print(F("Time: "));
  Serial.println(getTimeout());  
  Serial.print(F("State: "));
  Serial.println((doPlay ? "PLAY" : "PAUSE"));  
  Serial.print(F("Logging is "));
  Serial.println((doLog ? "ON" : "OFF"));  
}

void paramsRead() {
  // Setting default values
  setVel(VEL);
  setAmp(MAX_AMP);
  setDec(DEC_VEL);
  setOff(OFFSET);
  setWsp(WHEEL_SPD);
  setAcc(WHEEL_ACC);
  setMod(MODE);
  setTimeout(TIMEOUT_SECONDS);
  // reading from EEPROM if possible
  Serial.println("Reading params from EEPROM.");
  if (EEPROM.read(NUMBER_OF_PARAMS) == NUMBER_OF_PARAMS) {
    int pos = 0;
    setVel(EEPROM.read(pos++)); // Movements per second
    setAmp(EEPROM.read(pos++));
    setDec(EEPROM.read(pos++));
    setOff(EEPROM.read(pos++));
    setWsp((signed char)EEPROM.read(pos++));
    setAcc(EEPROM.read(pos++));
    setMod(EEPROM.read(pos++));
    setTimeout(EEPROM.read(pos++));
  } else {
    Serial.println("EEPROM values not valid. Setting to default values.");
  }
}

void paramsSave() {
  int pos = 0;
  Serial.println("Saving params to EEPROM.");
  EEPROM.write(NUMBER_OF_PARAMS, 0); // Invalidate EEPROM content;
  EEPROM.write(pos++, (unsigned char)getVel());
  EEPROM.write(pos++, (unsigned char)getAmp());
  EEPROM.write(pos++, (unsigned char)getDec());  
  EEPROM.write(pos++, (unsigned char)getOff());  
  EEPROM.write(pos++, (signed char)getWsp());  
  EEPROM.write(pos++, (unsigned char)getAcc());  
  EEPROM.write(pos++, (unsigned char)getMod());  
  EEPROM.write(pos++, (unsigned char)getTimeout());  
  EEPROM.write(pos, pos); // Validate EEPROM content;
}

