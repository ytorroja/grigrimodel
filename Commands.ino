void commandsSetup() {
  SCmd.addCommand("tim", setTimeout);       // Sets timeout
  SCmd.addCommand("spd", setSpeed);         // Sets speed
  SCmd.addCommand("amp", setAmplitude);     // Sets amplitude
  SCmd.addCommand("dec", setDecay);         // Sets decay
  SCmd.addCommand("off", setOffset);        // Sets offset
  SCmd.addCommand("wsp", setWheelSpeed);    // Sets wheel max speed
  SCmd.addCommand("acc", setWheelAccel);    // Sets whhel acceleration
  SCmd.addCommand("pps", togglePlayPause);  // Toggles play pause
  SCmd.addCommand("mod", toggleMod);        // Toggles type of movement
  SCmd.addCommand("log", toggleLog);        // Toggles printing
  SCmd.addCommand("save", paramsSave);      // Save parameters
  SCmd.addDefaultHandler(printHelp);        // Prints help   
}

void printHelp() {
  Serial.println(F("\nCommands:"));
  Serial.println(F("tim xxx : Set interaction time in seconds 10-250 (ej: tim 30)"));
  Serial.println(F("spd xxx : Set movement speed in times per 100 seconds 1-200 (ej: spd 70)"));
  Serial.println(F("amp xxx : Set movement amplitude in degrees 0-90 (ej: amp 30)"));
  Serial.println(F("dec xxx : Set amplitude decay speed in parts per thousand 1-100 (ej: dec 50)"));
  Serial.println(F("off xxx : Set center position in degrees 0-180 (ej: off 90)"));
  Serial.println(F("wsp xxx : Set wheel max speed -100 to +100 (ej: wsp 30)"));
  Serial.println(F("acc xxx : Set wheel acceleration 1-200 (ej: acc 20)"));
  Serial.println(F("mod  : Toggles between linear and sinusoidal movement"));
  Serial.println(F("pps  : Toggles between play and pause (not saved)"));
  Serial.println(F("log  : Toggles printing of current position on terminal (not saved)"));
  Serial.println(F("save : Save parameters in EEPROM"));
  Serial.println(F("\nCurrent values:"));
  printParams();
}

void setTimeout() {
  int aNumber;  
  char *arg; 

  arg = SCmd.next(); 
  if (arg != NULL) 
  {
    aNumber = atoi(arg);    // Converts a char string to an integer
    aNumber = constrain(aNumber, 1, 250);
    Serial.print(F("Setting Time to ")); 
    Serial.println(aNumber); 
    setTimeout(aNumber);
  }    
}

void setSpeed() {
  int aNumber;  
  char *arg; 

  arg = SCmd.next(); 
  if (arg != NULL) 
  {
    aNumber = atoi(arg);    // Converts a char string to an integer
    aNumber = constrain(aNumber, 1, 200);
    Serial.print(F("Setting Speed to ")); 
    Serial.println(aNumber);
    setVel(aNumber);
  }   
}

void setAmplitude() {
  int aNumber;  
  char *arg; 

  arg = SCmd.next(); 
  if (arg != NULL) 
  {
    aNumber = atoi(arg);    // Converts a char string to an integer
    aNumber = constrain(aNumber, 0, 90);
    Serial.print(F("Setting Amplitude to ")); 
    Serial.println(aNumber); 
    setAmp(aNumber);
    currAmp = getAmp();
  } 
}

void setDecay() {
  int aNumber;  
  char *arg; 

  arg = SCmd.next(); 
  if (arg != NULL) 
  {
    aNumber = atoi(arg);    // Converts a char string to an integer
    aNumber = constrain(aNumber, 1, 100);
    Serial.print(F("Setting Decay Speed to ")); 
    Serial.println(aNumber);
    setDec(aNumber);
  }   
}

void setOffset() {
  int aNumber;  
  char *arg; 

  arg = SCmd.next(); 
  if (arg != NULL) 
  {
    aNumber = atoi(arg);    // Converts a char string to an integer
    aNumber = constrain(aNumber, 0, 180);
    Serial.print(F("Setting Offset to ")); 
    Serial.println(aNumber);
    setOff(aNumber);
    if (state == WAITING) currPos = offset;
  }   
}


void toggleMod() {
  mod = !mod;
  Serial.print(F("Setting mod to "));
  Serial.println((mod ? "LIN" : "SIN"));  
}

void setWheelAccel() {
  int aNumber;  
  char *arg; 

  arg = SCmd.next(); 
  if (arg != NULL) 
  {
    aNumber = atoi(arg);    // Converts a char string to an integer
    aNumber = constrain(aNumber, 1, 200);
    Serial.print(F("Setting Wheel Accel to ")); 
    Serial.println(aNumber);
    setAcc(aNumber);
  }   
}

void setWheelSpeed() {
  int aNumber;  
  char *arg; 

  arg = SCmd.next(); 
  if (arg != NULL) 
  {
    aNumber = atoi(arg);    // Converts a char string to an integer
    aNumber = constrain(aNumber, -100, 100);
    Serial.print(F("Setting Wheel Max Speed to ")); 
    Serial.println(aNumber);
    setWsp(aNumber);
  }   
}

void togglePlayPause() {
  doPlay = !doPlay;  
  Serial.print(F("Setting play/pause to "));
  Serial.println((doPlay ? "PLAY" : "PAUSE"));  
}

void toggleLog() {
  doLog = !doLog;  
  Serial.print(F("Setting log to "));
  Serial.println((doLog ? "ON" : "OFF"));  
}

void saveParms() {
  Serial.println(F("Saving parameters to flash"));
  paramsSave();
}


