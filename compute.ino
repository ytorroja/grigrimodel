float lin(float t) {
  float v = (t + PI) / TWO_PI; 
  v = 4 * (v - floor(v));
  if (v >= 3) return  v - 4;
  if (v >= 1) return  2 - v;
  return v;
}

float getPosition(unsigned long deltaT, States * state) {
  if (*state == RUNNING) {
    currAmp = getAmp();
  }
  if (*state == STOPPING) {
    currAmp = currAmp * (1.0 - dec);
    if (currAmp <= 1) {
      currAmp = 0;
      // *state = WAITING;
    }
  }

  if (!doPlay) {
    *state = WAITING;
    currAmp = 0;
    return offset;
  }
  
  int sign = (wsp <= 90 ? 1 : -1);
  
  float omegaT = sign * TWO_PI * deltaT / 1000.0 * vel;
  
  if (mod) {
    return (offset + lin(omegaT) * currAmp);  
  } else {
    return (offset + sin(omegaT) * currAmp);  
  }
  
}

float getWheel(unsigned long deltaT, States * state) {
  if (*state == RUNNING) {
    int sign = (wsp >= 90 ? 1 : -1);
    float posInc = (currPos - oldCurrPos) * sign;
    if (posInc > 0.0) {
      currVel = currVel + posInc * acc * sign;
      if (wsp > 90) {
        if (currVel > wsp) currVel = wsp;
      } else{
        if (currVel < wsp) currVel = wsp;
      }
    }
    oldCurrPos = currPos;
  } else {
    currVel = 90 + (currVel - 90) * (1.0 - dec);
    if (*state == STOPPING) {
      if (abs(currVel-90) < 1) {
        *state = WAITING;
        currAmp = 0;
      }
    }
  }

  return currVel;
}

