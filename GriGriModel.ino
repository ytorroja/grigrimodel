#include <Servo.h>
#include <SerialCommand.h>

#define BALANCIN     9  // Pin for the "balancin" servo
#define RUEDA        11 // Pin for the wheel servo
#define LED          13 // Led pin
#define SENSOR       8  // Pin for the presence detection sensor

Servo balancin;      // create servo object to control a servo
Servo wheel;         // create servo object to control a servo
SerialCommand SCmd;  // The demo SerialCommand object

// Default values
#define TIMEOUT_SECONDS 10  // Interaction time
#define VEL             50  // Speed of movement
#define MAX_AMP         30  // Amplitude
#define OFFSET          90  // Offset
#define DEC_VEL         10  // Speed of change in 1/1000 (for starting and stopping interaction)
#define WHEEL_SPD       40  // Wheel Max Sppeed
#define WHEEL_ACC       80  // Wheel acceleration
#define MODE            0   // Sinusoidal or linear movement

unsigned long timeout_ms; // Interaction time in millis
unsigned long timeOn;     // Interaction starting time
unsigned long timeOff;    // Programmed stopping time
unsigned long deltaTms;   // Current interaction lapsed time

float amp;    // Amplitude of movement in degress
float vel;    // Cycles per second
float dec;    // Spped of amplitud change in starting and stopping interaction
float offset; // Center (or stop) position
float wsp;    // Wheel acceleration
float acc;    // Wheel acceleration
boolean mod   = false;  // controls type of movement

float currPos;  // Current postion of servo in degrees
float currAmp;  // Current amplitude of movement in degress (for starting and stopping effect)
float currVel;  // Current whhel speed

float oldCurrPos; // To compute pos increments

boolean doLog  = false;  // controls debugging information printing
boolean doPlay = true;   // controls play/pause

typedef enum {WAITING, RUNNING, STOPPING} States;

States state;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  balancin.attach(BALANCIN);
  wheel.attach(RUEDA);
  pinMode(SENSOR, INPUT);
  pinMode(LED, OUTPUT);
  
  paramsRead();
  printHelp();

  currPos = offset;
  currAmp = amp;
  currVel = 90;
  
  balancin.write(offset);
  wheel.write(90);

  commandsSetup();
}


void loop() {
  digitalWrite(LED, digitalRead(SENSOR));
 
  switch(state) {
    case WAITING:
      if (digitalRead(SENSOR)) {
        timeOn  = millis();
        timeOff = timeOn + timeout_ms;
        state   = RUNNING;
      }
      break;
    case RUNNING:
      if (millis() > timeOff) {
        state = STOPPING;
      }
      deltaTms = millis() - timeOn;
      currPos = getPosition(deltaTms, &state);
      balancin.write(currPos);
      break;
    case STOPPING:
      deltaTms = millis() - timeOn;
      currPos = getPosition(deltaTms, &state);
      balancin.write(currPos);
      /*
      if (digitalRead(SENSOR)) {
        timeOff = millis() + timeout_ms;
        state   = RUNNING;
      }
      */
      break;
  }

  currVel = getWheel(deltaTms, &state);
  wheel.write(currVel);
      
  if (doLog) {
    Serial.print(digitalRead(SENSOR));
    Serial.print(" ");
    Serial.print(state);
    // Serial.print(" ");
    // Serial.print(deltaTms);
    Serial.print(" ");
    Serial.print(currAmp);
    Serial.print(" ");
    Serial.print(currVel);
    Serial.print(" ");
    Serial.println((int)currPos);
  }

  SCmd.readSerial();

  delay(10);

}
